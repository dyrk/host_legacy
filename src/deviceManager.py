import yaml
import time
import json
from paho.mqtt import client as mqtt_client
from typing import List
import logging

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='[%H:%M:%S]', level=logging.DEBUG)

class DeviceManager:
    """DeviceManager controls devices and device communication."""

    def __init__(self):
        with open("hostconfig.yml", "r") as ymlfile:
            self.host_config = yaml.safe_load(ymlfile)
        self.client = self._connect_mqtt(self.host_config['mqtt']['broker'])
        self.client.loop_start()
        
        self.start_time = int(time.time())

    def attach_databaseManager(self, databaseManager: object):
        """Attaches databaseManager to deviceManager for logging data.
        Directly attaches deviceManager. Should be abstracted to IPC.

        Args:
            databaseManager (object): DatabaseManager.
        """

        self.databaseManager = databaseManager
    
    def insert_data_into_database(self, collection: str, data_event: dict):
        """Inserts an entry into a collection in the database.

        Args:
            collection (str): ID of the collection.
            data_event (dict): Data to be entered.
        """

        self.databaseManager.insert_data(collection, data_event)

        """Private function for connecting to mqtt broker"""
    def _connect_mqtt(self, broker_configuration: dict) -> object:
        """Sets up a connection to an mqtt broker.

        Args:
            broker_configuration (dict): Configuration with basic connection information.

        Returns:
            [object]: Client object for interacting with broker.
        """

        broker = broker_configuration['endpoint']
        port = broker_configuration['port']
        client_id = broker_configuration['client_id']
        username = broker_configuration['username']
        password = broker_configuration['password']
        
        def on_connect(self, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        # Set Connecting Client ID
        client = mqtt_client.Client(client_id)
        client.username_pw_set(username, password)
        client.on_connect = on_connect
        client.connect(broker, port)

        return client

    def subscribe_to_topic(self, topic: str):
        """Adds a topic to deviceManagers subscriptions.

        Args:
            topic (str): Topic to subscribe to.
        """

        logging.info(f"Subscribed to topic: {topic}")

        def on_message(client, userdata, msg):
            try:
                eventstring = msg.payload.decode()
                parsedevent = json.loads(eventstring)
                logging.info(f"[{msg.topic}] Received an event: {parsedevent}")
                if parsedevent['event_type'] == 'data_event':
                    self.insert_data_into_database(msg.topic, parsedevent)
                elif parsedevent['event_type'] == 'enroll_event':
                    pass
                elif parsedevent['event_type'] == 'output_event':
                    pass
                else:
                    logging.info(f"Unknown event type received: {parsedevent['event_type']}")
            except ValueError:
                logging.info("String could not be converted to JSON.")

        self.client.subscribe(topic)
        self.client.on_message = on_message

    def enroll_device_in_plan(self, device_id: str, topic: str):
        """Tells a device to join a mqtt topic. 

        Args:
            device_id (str): ID of device to enroll.
            topic (str): Topic device should subscribe to.
        """
        enroll_event = {
            "event_type": 'enroll_event',
            "device_id": device_id,
            "plan_id": topic
        }
        eventstring = json.dumps(enroll_event)
        self.client.publish(device_id+"_command", eventstring)
        
    def send_event_to_topic(self, topic: str, event: dict):
        """Sends event to topic.

        Args:
            topic (str): Topic to send event to.
            event (dict): Event to send.
        """

        eventstring = json.dumps(event)
        logging.debug(f"Sending: {eventstring}")
        self.client.publish(topic, eventstring)

    def get_connected_devices(self) -> List[str]:
        """Returns a list of all devices connected to host.

        Returns:
            List[str]: List of IDs for connected devices.
        """

        return self.host_config['deviceManager']['devices']

if __name__ == '__main__':
    pass
