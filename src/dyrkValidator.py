"""Contains functions for validating different types of dyrk objects, such as events and plans."""

def validate_object(json_object: dict) -> bool:
    """Checks whether an object follow a valid dyrk specification

    Args:
        json_object (dict): Object to validate.

    Returns:
        bool: Whether or not object is valid.
    """

    return True


def validate_schedule_plan(plan: dict) -> bool:
    """Validates that dictionary is a dyrk schedule plan.

    Args:
        plan (dict): Plan to validate.

    Returns:
        bool: Whether or not plan is valid.
    """

    return True