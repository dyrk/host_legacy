from pymongo import MongoClient
import yaml
from typing import List
# pprint library is used to make the output look more pretty
from pprint import pprint
import logging

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='[%H:%M:%S]', level=logging.DEBUG)

class DatabaseManager:
    """DatabaseManager controls connection to mongodb"""

    def __init__(self):
        with open("hostconfig.yml", "r") as ymlfile:
            self.host_config = yaml.safe_load(ymlfile)
        self.connect_to_mongodb()
        db_id = self.host_config['mqtt']['broker']['client_id']
        self.db = self.client.db_id

    def connect_to_mongodb(self):
        """Creates a connection client to mongodb on self.client"""
        endpoint = self.host_config['databaseManager']['endpoint']
        port = self.host_config['databaseManager']['port']
        self.client = MongoClient(endpoint, port)
        
    def get_all_from_collection(self, collection: str) -> List[object]:
        """Gets all documents from a collection.

        Args:
            collection (str): ID of collection.

        Returns:
            List[object]: List of documents.
        """
        
        cursor = self.db[collection].find({})
        
        return list(cursor)

    def insert_data(self, collection: str, document: dict):
        """Inserts a single document into a collection.

        Args:
            collection (str): ID of collection.
            document (dict): Document to insert.
        """

        self.db[collection].insert_one(document)
    
    def drop_collection(self, collection: str):
        """Drops a collection from the database

        Args:
            collection (str): ID of collection.
        """
        self.db.drop_collection(collection)

if __name__ == '__main__':
    pass