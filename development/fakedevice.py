# Simulates a device of some kind. This will be removed when device code is ready.
from paho.mqtt import client as mqtt_client
import yaml
import json
import logging

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='[%H:%M:%S]', level=logging.DEBUG)

class FakeDyrkDevice:

    def __init__(self):
        with open("deviceconfig.yml", "r") as ymlfile:
            self.device_config = yaml.safe_load(ymlfile)
        self.client = self.connect_mqtt(self.device_config)
        self.command_channel_id = self.device_config['mqtt']['broker']['client_id']+"_command"
        self.data_channel_id = self.device_config['mqtt']['broker']['client_id']+"_data"
        self.subscribe_to_topic(self.command_channel_id)
        self.subscribe_to_topic(self.data_channel_id)

    def connect_mqtt(self, configuration):
        broker = configuration['mqtt']['broker']['endpoint']
        port =   configuration['mqtt']['broker']['port']
        # generate client ID with pub prefix randomly
        client_id = configuration['mqtt']['broker']['client_id']
        username = configuration['mqtt']['broker']['username']
        password = configuration['mqtt']['broker']['password']

        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                logging.info("Connected to MQTT Broker!")
            else:
                logging.info("Failed to connect, return code %d\n", rc)

        # Set Connecting Client ID
        client = mqtt_client.Client(client_id)
        client.username_pw_set(username, password)
        client.on_connect = on_connect
        client.connect(broker, port)

        return client

    def set_output(self, output_event):
        logging.info(f"Setting output: {output_event['output_amount']}")
        pass

    def enroll_in_plan(self, plan_id):
        logging.info(f"Enrolling in plan: {plan_id}")
        self.subscribe_to_topic(plan_id)

    def subscribe_to_topic(self, topic):
        logging.info(f"Subscribing to topic: {topic}")

        def on_message(client, userdata, msg):
            try:
                eventstring = msg.payload.decode()
                parsedevent = json.loads(eventstring)
                logging.info(f"[{msg.topic}] Received an event: {parsedevent}")
                if parsedevent['event_type'] == 'enroll_event':
                    self.enroll_in_plan(parsedevent['plan_id'])
                elif parsedevent['event_type'] == 'output_event':
                    self.set_output(parsedevent)
                elif parsedevent['event_type'] == 'data_event':
                    pass
                else:
                    logging.info(f"Unknown event type received: {parsedevent['event_type']}")
            except ValueError:
                logging.info("String could not be converted to JSON.")

        self.client.subscribe(topic)
        self.client.on_message = on_message
    
    def send_data(self):
        from random import randint
        import time
        data_event = {
            "event_type": "data_event",
            "timestamp": int(time.time()),
            "data": {
                "temperature": randint(10, 30),
                "pressure": randint(0, 100),
                "humidity": randint(0, 100),
            }
        }
        eventstring = json.dumps(data_event)
        self.client.publish(self.data_channel_id, eventstring)

if __name__ == "__main__":
    import time
    FDD = FakeDyrkDevice()
    FDD.client.loop_start()

    while True:
        FDD.send_data()
        time.sleep(1)
