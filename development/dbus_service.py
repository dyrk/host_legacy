from pydbus import SystemBus

# Send a command to list all running dbus system services
# dbus-send --system --print-reply --dest=org.freedesktop.DBus  /org/freedesktop/DBus org.freedesktop.DBus.ListNames

# Configuration file for dyrkhost dbus system service
# /etc/dbus-1/system.d/dyrkhost-dbus.conf

# Send a hello world command
# dbus-send --system --print-reply --dest=org.freedesktop.dyrkhost  /org/freedesktop/dyrkhost org.freedesktop.dyrkhost.Hello

from gi.repository import GLib

loop = GLib.MainLoop()

class MyDBUSService(object):
	"""
		<node>
			<interface name='org.freedesktop.dyrkhost'>
				<method name='Hello'>
					<arg type='s' name='response' direction='out'/>
				</method>
				<method name='EchoString'>
					<arg type='s' name='a' direction='in'/>
					<arg type='s' name='response' direction='out'/>
				</method>
				<method name='Quit'/>
			</interface>
		</node>
	"""

	def Hello(self):
		"""returns the string 'Hello, World!'"""
		print("Hello() called")
		return "Hello, World!"

	def EchoString(self, s):
		"""returns whatever is passed to it"""
		print(f"EchoString({s}) called")
		return s

	def Quit(self):
		"""removes this object from the DBUS connection and exits"""
		loop.quit()

bus = SystemBus()
bus.publish("org.freedesktop.dyrkhost", MyDBUSService())
loop.run()