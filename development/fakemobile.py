import requests
from pprint import pprint

import logging

import sys
sys.path.append("/workspace/development")

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='[%H:%M:%S]', level=logging.INFO)

class FakeDyrkMobile:
    
    def __init__(self) -> None:
        pass

    def get_host_config(self):
        logging.info(f"Getting host configuration:")
        response = requests.get("http://127.0.0.1/gethostinfo")
        return response.json()
    
    def get_host_plans(self):
        logging.info(f"Getting plans installed on host")
        response = requests.get("http://127.0.0.1/getplans")
        return response.json()

    def get_host_devices(self):
        logging.info(f"Getting available host devices")
        response = requests.get("http://127.0.0.1/getdevices")
        return response.json()

    def post_new_plan(self, plan):
        logging.info(f"Posting new plan to host")
        response = requests.post('http://127.0.0.1/uploadplan', json=plan)
        return response.json()

    def start_plan(self, plan_run_configuration):
        logging.info(f"Requesting host to start plan")
        response = requests.post('http://127.0.0.1/startplan', json=plan_run_configuration)
        return response.json()


if __name__ == '__main__':
    import sys
    import createplan
    import json
    FDM = FakeDyrkMobile()

    r = FDM.get_host_config()
    pprint(r)

    r = FDM.get_host_devices()
    pprint(r)

    r = FDM.get_host_plans()
    pprint(r)

    plan_filename = "fakemobileplan.json"
    created_plan = createplan.create_plan_file(plan_filename, 0)
    
    with open("fakemobileplan.json") as f:
        created_plan = json.load(f)

    r = FDM.post_new_plan(created_plan)
    pprint(r)

    r = FDM.get_host_plans()
    pprint(r)

    plan_run_configuration = {
        "event_type": "plan_run_configuration",
        "configuration": {
            "start_time": 100,
            "plan_id": created_plan['plan_id'],
            "devices": ['dyrk_DEVICE_0', 'dyrk_DEVICE_1', 'dyrk_DEVICE_2', 'dyrk_DEVICE_3']
        }
    }

    r = FDM.start_plan(plan_run_configuration)
    pprint(r)

    r = FDM.get_host_plans()
    pprint(r)