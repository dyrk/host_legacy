import time
import yaml
import json
import glob

from typing import List
from ..src.dyrkValidator import *

import logging

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='[%H:%M:%S]', level=logging.DEBUG)

class PlanManager:
    """PlanManager looks into active plan at regular intervals.
    Sends out events from plans if needed"""

    def __init__(self):
        with open("hostconfig.yml", "r") as ymlfile:
            self.host_config = yaml.safe_load(ymlfile)
        self.active_plans = []

    def check_for_action(self, timestamp: int, active_plans: List[dict]):
        """Iterates through all active plans and signals deviceManager to 
        sends out events to devices.

        Args:
            timestamp (int): Timestamp to check for action.
            active_plans (List[dict]): List of active plan dicts.
        """

        for plan in active_plans:
            # If there is an event at this timestamp, send output_event to plan channel
            events = plan['events']
            event_at_timestamp = False
            for event in events:
                event_timestamp = plan['start_time'] + event["relative_timestamp"]
                if (event_timestamp == timestamp):
                    self.send_event_to_plan_channel(plan['plan_id'], event)
                    event_at_timestamp = True
            if not event_at_timestamp:
                logging.debug(f"No action needed for plan: {plan['plan_id']}")
                event_at_timestamp = False

    def attach_deviceManager(self, deviceManager: object):
        """Attaches deviceManager to planManager for sending events to devices.
        Directly attaches planManager. Should be abstracted to IPC.

        Args:
            deviceManager (object): DeviceManager.
        """

        self.deviceManager = deviceManager

    def send_event_to_plan_channel(self, plan_channel_id: str, event: dict):
        """Sends event to plan channel through deviceManager

        Args:
            plan_channel_id (str): Target plan channel to send event to.
            event (dict): Event to be executed to plan subscribers.
        """

        self.deviceManager.send_event_to_topic(plan_channel_id, event)

    def get_plans(self) -> List[str]:
        """Returns IDs for all available plans.
        Temporary replacement for databasemanager using filesystem.

        Returns:
            [List[str]]: List of IDs for available plans.
        """

        plans = [file.split("/")[1].split('.')[0] for file in glob.glob(f"{self.host_config['planManager']['plan_folder']}/*.json")]

        return plans

    def add_plan_to_plans(self, plan: dict) -> str:
        """Add plans to local host database.
        Temporary replacement for databasemanager using filesystem.
        Does not check for duplicates!

        Args:
            plan (dict): Plan object in dictionary form.

        Returns:
            [str]: Confirmation message that plan was added.
        """

        plan_id = plan['plan_id']
        filepath = f"{self.host_config['planManager']['plan_folder']}/{plan_id}.json"
        with open(filepath, 'w') as f:
            json.dump(plan, f)
        
        return f"Plan with ID: {plan['plan_id']} added."

    def start_plan(self, plan_run_configuration: dict) -> str:
        """Starts a plan on host and attached devices.

        Args:
            plan_run_configuration (dict): Configuration for running plan.

        Returns:
            str: Confirmation message that plan is started.
        """

        plan_id = plan_run_configuration['configuration']['plan_id']

        filepath = f"{self.host_config['planManager']['plan_folder']}/{plan_id}.json"

        with open(f"{filepath}") as json_file:
            plan_dict = json.load(json_file)
            plan_dict['start_time'] = int(time.time())  # Start time should be in a plan_run_configuration
            self.active_plans.append(plan_dict)
        
        return f"Plan with ID: {plan_id} and start_time: {plan_dict['start_time']} added to active_plans."

    def load_plan(self, filepath: str) -> dict:
        """Loads plan from filesystem.

        Args:
            filepath (string): Path to file in filesystem.

        Returns:
            dict: Loaded plan as dictionary.
        """

        with open(f"{filepath}") as json_file:
            plan_data = json.load(json_file)
            if dyrkValidator.validate_schedule_plan(plan_data):
                return plan_data
        
    def get_events_from_plan(self, plan_id: str) -> List[dict]:
        """Returns a list of events from a plan_id.

        Args:
            plan_id (str): ID of plan.

        Returns:
            List[dict]: List of events.
        """

        filepath = f"{self.host_config['planManager']['plan_folder']}/{plan_id}.json"
        plan = self.load_plan(filepath)
        events = plan['events']

        return events

if __name__ == "__main__":
    pass
