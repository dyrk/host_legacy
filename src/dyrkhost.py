#!/usr/local/bin/python3
"""This is a combination of all dyrk host applications"""
import time
import json
from typing import List

from src.deviceManager import DeviceManager
from src.planManager import PlanManager
from src.databaseManager import DatabaseManager

import logging

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='[%H:%M:%S]', level=logging.DEBUG)

import zmq

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5555")

class DyrkMainController:
    def __init__(self):
        self.DatabaseManager = DatabaseManager()
        self.DeviceManager = DeviceManager()
        self.DeviceManager.attach_databaseManager(self.DatabaseManager)
        self.PlanManager = PlanManager()
        self.PlanManager.attach_deviceManager(self.DeviceManager)

        self.connected_devices = self.DeviceManager.get_connected_devices()
        logging.info(f"Connected devices: {self.connected_devices}")

        for target_device in self.connected_devices:
            self.DeviceManager.subscribe_to_topic(target_device+"_command")
            self.DeviceManager.subscribe_to_topic(target_device+"_data")

    def wrap_response(self, response_message: str, response_data: List[dict]=[]) -> str:
        """Wraps responses from host into a main_controller_response type.

        Args:
            response_message (string): Response message.
            response_data (List[str], optional): Data for response. Defaults to [].

        Returns:
            string: Wrapped response in json format.
        """
        rd = {
            "event_type": "main_controller_response",
            "response": {
                "message": response_message,
                "data": response_data
            }
        }
        return json.dumps(rd).encode("utf-8")

    def enroll_device(self, device: str, plan: str):
        """Enroll a device to a plan.

        Args:
            device (str): Device ID.
            plan (str): Plan ID.
        """
        
        logging.info(f"Enrolling device: {device} to plan: {plan}")
        self.DeviceManager.enroll_device_in_plan(device, plan)
        self.DeviceManager.subscribe_to_topic(plan)
    
    def save_new_plan(self, plan: dict) -> str:
        logging.info(f"Saving plan with ID: {plan['plan_id']}")
        response = self.PlanManager.add_plan_to_plans(plan)
        return response

    def start_plan(self, plan_run_configuration):
        import sys

        devices = plan_run_configuration['configuration']['devices']
        plan_id = plan_run_configuration['configuration']['plan_id']

        # Demo function - will enroll ALL available devices into this plan
        for target_device in devices:
            self.enroll_device(target_device, plan_id)
        response = self.PlanManager.start_plan(plan_run_configuration)
        return response
    


if __name__ == "__main__":
    DMC = DyrkMainController()

    # Main program loop
    while True:
        try:
            #  Wait for next request from frontend
            message = socket.recv(flags=zmq.NOBLOCK)
            logging.info(f"Received message: {message}")
            parsed_object = json.loads(message.decode())
            command_message = parsed_object['request']['message']

            if command_message == "get_devices()":
                response = DMC.wrap_response(response_message="get_devices()", response_data=DMC.DeviceManager.get_connected_devices())
                socket.send(response)
            elif command_message == "get_plans()":
                response = DMC.wrap_response(response_message="get_plans()", response_data=DMC.PlanManager.get_plans())
                socket.send(response)
            elif command_message == "get_hostinfo()":
                response = DMC.wrap_response(response_message="get_plans()", response_data="SOMEHOSTINFO")
                socket.send(response)
            elif command_message == "upload_plan()":
                response = DMC.wrap_response(response_message="upload_plan()", response_data=DMC.save_new_plan(parsed_object['request']['data']))
                socket.send(response)
            elif command_message == "start_plan()":
                response = DMC.wrap_response(response_message="start_plan()", response_data=DMC.start_plan(parsed_object['request']['data']))
                socket.send(response)

        except zmq.ZMQError as e:
            if e.errno == zmq.EAGAIN:
                pass # no message was ready (yet!)

        current_timestamp = int(time.time())
        DMC.PlanManager.check_for_action(current_timestamp, DMC.PlanManager.active_plans)
        time.sleep(DMC.PlanManager.host_config["planManager"]['audit_interval'])