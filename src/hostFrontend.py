try:
    # checks if you have access to RPi.GPIO, which is available inside RPi
    import RPi.GPIO as GPIO
except ImportError:
    # In case of exception, you are executing your script outside of RPi, so import Mock.GPIO
    import Mock.GPIO as GPIO

from flask import Flask, request
import json
import os
import glob
import requests
from requests.models import Response
from ..src.dyrkValidator import *
import yaml
import zmq
import sys


import logging

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='[%H:%M:%S]', level=logging.INFO)

app = Flask("application")

### ZEROMQ
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:5555")


with open("hostconfig.yml", "r") as ymlfile:
    host_config = yaml.safe_load(ymlfile)

### --- STATIC --- ###
def wrap_response(response_message, response_data=[]):
    """Wraps responses from host into a host_response_event type.

    Args:
        response_message (string): Response message.
        response_data (list, optional): Data for response. Defaults to [].

    Returns:
        string: Wrapped response in json format.
    """
    rd = {
        "event_type": "host_response_event",
        "response": {
            "message": response_message,
            "data": response_data
        }
    }
    return json.dumps(rd)


def save_plan_to_folder(plan, filename):
    """Saves a plan to the plans folder.

    Args:
        plan (string): dyrkplan parsed to dictionary.
        filename (type): Filename for plan, does not need full path.

    Returns:
        boolean: Returns False if file already exists.
    """
    folder_for_plans = host_config['planManager']['plan_folder']
    if not os.path.exists(folder_for_plans):
        os.makedirs(folder_for_plans)
    full_path = f'{folder_for_plans}/{filename}.json'

    if (os.path.exists(full_path)):
        print("File already there")
        return False
    else:
        with open(full_path, 'w') as fp:
            json.dump(plan, fp)
        return True

def get_plans_from_directory(directory):
    """Lists all plans in directory (no validation, just lists .json)

    Args:
        directory (string): Path to target directory.

    Returns:
        list: List of strings with paths for plans.
    """
    socket.send_string("get_plans_from_directory()")
    message = socket.recv()
    print(message)


    plans = [file for file in glob.glob(f"{directory}/*.json")]

    return plans
### --- /STATIC --- ###

def send_zmq_message(message, data={}):
    # Sends and receives json
    def wrap_message(message, data):
        """Wraps message in a host_frontend_request"""
        rd = {
            "event_type": "host_frontend_request",
            "request": {
                "message": message,
                "data": data
            }
        }
        return json.dumps(rd)

    request = wrap_message(message, data)
    logging.info(f"Sending message: {request}")
    socket.send_string(request)
    string_response = socket.recv().decode()
    logging.info(f"Received response: {string_response}")
    return string_response

### --- ROUTES --- ###
@app.route("/uploadplan", methods=['POST'])
def upload_plan():
    """Uploads plan to host. Validates and saves to persistent memory.
    
    Test POST with: 
        curl -X POST -H "Content-Type: application/json" -d @./exampleplan.json http://127.0.0.1/uploadplan
    """
    incoming_plan = request.get_json()
    response = send_zmq_message("upload_plan()", incoming_plan)
    return_object = wrap_response("Response", response)

    return return_object

@app.route("/startplan", methods=['POST'])
def start_plan():
    """Starts a plan with a specific id on the host.

    Returns:
        [type]: [description]
    """
    plan_id = request.get_json()
    plan_started = send_zmq_message("start_plan()", plan_id)
    return_object = wrap_response("Started plan", plan_started)

    return return_object

@app.route("/getdevices", methods=['GET'])
def get_devices():
    """Returns information about what devices are available to the host
    Test GET with: 
        curl http://127.0.0.1/getdevices

    Returns:
        string[]: List of devices.
    """
    connected_devices = send_zmq_message("get_devices()")
    return_object = wrap_response("Connected devices", connected_devices)

    return return_object

@app.route("/getplans", methods=['GET'])
def get_plans():
    """Returns information about what plans are available on host
    Test GET with: 
        curl http://127.0.0.1/getplans

    Returns:
        [type]: [description]
    """
    available_plans = send_zmq_message("get_plans()")
    return_object = wrap_response("Installed plans", available_plans)
    
    return return_object

@app.route("/gethostinfo", methods=['GET'])
def get_hostinfo():
    """Returns information about host
    Test GET with: 
        curl http://127.0.0.1/gethostinfo

    Returns:
        [type]: [description]
    """
    hostinfo = send_zmq_message("get_hostinfo()")
    return_object = wrap_response("Host device hostname", hostinfo)

    return return_object
### --- /ROUTES --- ###

if __name__ == "__main__":
    try:
        app.run(host="0.0.0.0", port=80, debug=True)

    except zmq.ZMQError as e:
        if e.errno == zmq.EAGAIN:
            pass # no message was ready (yet!)
