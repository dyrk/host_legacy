from pydbus import SystemBus

# Connect to the system bus
bus = SystemBus()

# Connect to dyrkhost on system bus
dyrkhost = bus.get('org.freedesktop.dyrkhost')

# Call the Hello function
dyrkhost.Hello()

# Get a reply from the EchoString function
reply = dyrkhost.EchoString("This message")
print(reply)