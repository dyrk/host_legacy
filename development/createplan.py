#!/usr/local/bin/python
import json
import uuid
import yaml
from random import randint


def create_data_from_property(property):
    """Creates a sample of data from a property"""
    if property['type'] == 'array':
        array_length = property['maxItems']
        if property['items']['type'] == 'string':
            return ["ml" for i in range(array_length)]
        elif property['items']['type'] == 'integer':
            minimum = property['items']['minimum']
            maximum = property['items']['maximum']
            return [randint(minimum, maximum) for i in range(array_length)]
    else:
        if property['type'] == 'string':
            return "ml"
        elif property['type'] == 'integer':
            return randint(5, 10)


def create_event_from_type(type, timestamp_overwrite_value):
    """Create an event from an event type such as output_event"""
    rd = {}

    # Create a random example of each required property
    for p in type["properties"]:
        data_to_add = create_data_from_property(type["properties"][p])
        rd[p] = data_to_add

    rd['relative_timestamp'] = timestamp_overwrite_value

    return rd


def create_list_of_events(event_type_name, event_type_data, n_events, time_start):
    """Creates a list of events"""
    timestamp_delay = time_start
    timestamp_interval = 2
    list_of_events = []

    for _ in range(n_events):
        event = create_event_from_type(event_type_data, timestamp_delay)
        event['event_type'] = event_type_name
        list_of_events.append(event)
        timestamp_delay = timestamp_delay + timestamp_interval

    plandict = {
        "object_type": "schedule_plan",
        "name": "Example plan",
        "plan_id": str(uuid.uuid4()),
        "events": list_of_events,
        "comment": "This is just some example to get started, it does mean everything stays this way"
    }

    return plandict


def create_plan_file(filename="outputplan.json", timestamp_delay=0):
    # Open event type definition file
    with open("/workspace/src/event_types.yml", "r") as ymlfile:
        event_types = yaml.safe_load(ymlfile)

    event_type_name = 'output_event'
    event_type_data = event_types[event_type_name]

    list_of_events = create_list_of_events('output_event', event_type_data, 10, timestamp_delay)

    with open(filename, "w") as json_file:
        json.dump(list_of_events, json_file, indent=4)

    return list_of_events
